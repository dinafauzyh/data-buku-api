<?php

namespace App\Http\Controllers;

use App\Models\User;
use Cache;

class UserController extends Controller
{
    public function getAllUser()
    {
        $query = Cache::remember('user_all', 10 * 60, function () {
            return User::all();
        });

        $response = [
            'message' => 'Berhasil!',
            'data' => $query,
        ];

        return response()->json($response, 200);
    }
}
