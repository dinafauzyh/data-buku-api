<?php

namespace App\Http\Controllers;

use App\Models\Books;
use Cache;

class BookController extends Controller
{
    public function getBooks()
    {
        $books = Cache::remember('all_books', 10 * 60, function () {
            return Books::all();
        });

        $response = [
            'message' => 'Berhasil',
            'data' => $books,
        ];

        return response()->json($response, 200);
    }
}
