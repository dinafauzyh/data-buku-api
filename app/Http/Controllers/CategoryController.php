<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Cache;

class CategoryController extends Controller
{
    public function getCategories()
    {
        $categories = Cache::remember('all_categories', 10 * 60, function () {
            return Category::all();
        });

        $response = [
            'message' => 'Berhasil',
            'data' => $categories,
        ];

        return response()->json($response, 200);
    }
}
